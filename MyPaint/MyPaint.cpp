﻿// MyPaint.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "MyPaint.h"
#include <commdlg.h>
#include <CommCtrl.h>

#define MAX_LOADSTRING 100

// Global Variables:
HWND hwndMDIClient = NULL;
HWND hFrameWnd = NULL;
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK WndNonameProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void onNonameWindow(HWND hwnd);
LRESULT CALLBACK CloseProc(HWND hwndChild, LPARAM lParam);
bool displayColorDlg(HWND hwnd);
HWND CreateToolbar(HWND hWndParent);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MYPAINT, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MYPAINT));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateMDISysAccel(hwndMDIClient,&msg) && !TranslateAccelerator(hFrameWnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MYPAINT));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_MYPAINT);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
	RegisterClassExW(&wcex);

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndNonameProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MYPAINT));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDI_MYPAINT);
	wcex.lpszClassName = L"MDI NONAME";
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
	RegisterClassExW(&wcex);

	return 1;
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   hFrameWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hFrameWnd)
   {
      return FALSE;
   }

   ShowWindow(hFrameWnd, nCmdShow);
   UpdateWindow(hFrameWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
	case WM_SIZE:
	{
		UINT w, h;
		w = LOWORD(lParam);
		h = HIWORD(lParam);
		MoveWindow(hwndMDIClient, 0, 28, w, h-28, TRUE);
	}
		break;
	case WM_CREATE:
	{
		CLIENTCREATESTRUCT ccs;
		ccs.hWindowMenu = GetSubMenu(GetMenu(hWnd), 2);
		ccs.idFirstChild = 50001;
		hwndMDIClient = CreateWindow(L"MDICLIENT",
			(LPCTSTR)NULL,
			WS_CHILD | WS_CLIPCHILDREN | WS_VSCROLL | WS_HSCROLL,
			0, 0, 0, 0,
			hWnd,
			(HMENU)NULL,
			hInst,
			(LPVOID)&ccs);
		ShowWindow(hwndMDIClient, SW_SHOW);

		CreateToolbar(hWnd);
	}
		break;
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
			case ID_DRAW_ELLIPSE:
			case ID_DRAW_LINE:
			case ID_DRAW_RECTANGLE:
			case ID_DRAW_TEXT:
			{
				MessageBox(hWnd, L"Bạn đã chọn 1 công cụ vẽ", L"CAPTION", MB_OK);
			}
			break;
			case ID_DRAW_COLOR:
			{
				if (displayColorDlg(hWnd) == false)
				{
					MessageBox(hWnd, L"NO CHOOSE COLOR", L"Caption!", MB_OK);
				}
			}
				break;
			case ID_WINDOW_CASCADE:
			{
				SendMessage(hwndMDIClient, WM_MDICASCADE, 0, 0);
			}
				break;
			case ID_WINDOW_CLOSEALL:
			{
				EnumChildWindows(hwndMDIClient, (WNDENUMPROC)CloseProc, 0L);
			}
				break;
			case ID_WINDOW_TIDE:
			{
				SendMessage(hwndMDIClient, WM_MDITILE, 0, 0);
			}
				break;
			case ID_FILE_NEW:
			{
				onNonameWindow(hWnd);
			}
				break;
			case ID_FILE_SAVE:
			{
				MessageBox(hWnd, L"Bạn đã chọn Save", L"CAPTION", MB_OK);
			}
				break;
			case ID_FILE_OPEN:
			{
				MessageBox(hWnd, L"Bạn đã chọn Open", L"CAPTION", MB_OK);
			}
				break;
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
		return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

LRESULT CALLBACK WndNonameProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{

	return DefMDIChildProc(hWnd, message, wParam, lParam);
}

int nNoname = 0;
void onNonameWindow(HWND hwnd)
{
	MDICREATESTRUCT mdiCreate;
	mdiCreate.szClass = L"MDI NONAME";
	nNoname = nNoname + 1;
	WCHAR input[MAX_LOADSTRING];
	swprintf_s(input, L"Noname%d.drw", nNoname);
	mdiCreate.szTitle = input;
	mdiCreate.hOwner = hInst;
	mdiCreate.x = CW_USEDEFAULT;
	mdiCreate.y = CW_USEDEFAULT;
	mdiCreate.cx = CW_USEDEFAULT;
	mdiCreate.cy = CW_USEDEFAULT;
	mdiCreate.style = 0;

	mdiCreate.lParam = NULL;
	SendMessage(hwndMDIClient, WM_MDICREATE, 0, (LPARAM)(LPCREATESTRUCT)&mdiCreate);
}

LRESULT CALLBACK CloseProc(HWND hwndChild, LPARAM lParam)
{
	SendMessage(hwndMDIClient, WM_MDIDESTROY, (WPARAM)hwndChild, 0L);
	return 1;
}

bool displayColorDlg(HWND hwnd)
{
	CHOOSECOLOR cc;                 // common dialog box structure 
	static COLORREF acrCustClr[16]; // array of custom colors 
	//HWND hwnd;                      // owner window
	HBRUSH hbrush;                  // brush handle
	static DWORD rgbCurrent;        // initial color selection

									// Initialize CHOOSECOLOR 
	ZeroMemory(&cc, sizeof(cc));
	cc.lStructSize = sizeof(cc);
	cc.hwndOwner = hwnd;
	cc.lpCustColors = (LPDWORD)acrCustClr;
	cc.rgbResult = rgbCurrent;
	cc.Flags = CC_FULLOPEN | CC_RGBINIT;

	if (ChooseColor(&cc) == TRUE)
	{
		hbrush = CreateSolidBrush(cc.rgbResult);
		rgbCurrent = cc.rgbResult;
		return true;
	}
	return false;
}

HIMAGELIST hImageList = NULL;

HWND CreateToolbar(HWND hWndParent)
{
	// Declare and initialize local constants.
	const int ImageListID = 0;
	const int numButtons = 8;
	const int bitmapSize = 16;

	const DWORD buttonStyles = BTNS_AUTOSIZE;

	// Create the toolbar.
	HWND hWndToolbar = CreateWindow(TOOLBARCLASSNAME, NULL,
		WS_CHILD | TBSTYLE_WRAPABLE, 0, 0, 0, 0,
		hWndParent, NULL, hInst, NULL);
	
	if (hWndToolbar == NULL)
		return NULL;
	



	hImageList = ImageList_LoadImage(hInst, MAKEINTRESOURCE(IDB_BITMAP1), 16, 10, CLR_DEFAULT,
		IMAGE_BITMAP, LR_CREATEDIBSECTION | LR_DEFAULTSIZE | LR_SHARED | LR_LOADTRANSPARENT);
	HIMAGELIST hOld = (HIMAGELIST)SendMessage(hWndToolbar, TB_SETIMAGELIST, 0, (LPARAM)hImageList);


	// Sending this ensures iString(s) of TBBUTTON structs become tooltips rather than button text
	SendMessage(hWndToolbar, TB_SETMAXTEXTROWS, 0, 0);

	DWORD dwCountImages = SendMessage(hWndToolbar, TB_LOADIMAGES, (WPARAM)IDB_STD_SMALL_COLOR, (LPARAM)HINST_COMMCTRL);



	// Initialize button info.
	// IDM_NEW, IDM_OPEN, and IDM_SAVE are application-defined command constants.

	TBBUTTON tbButtons[numButtons] =
	{
		{ dwCountImages + STD_FILENEW,	ID_FILE_NEW,		TBSTATE_ENABLED, buttonStyles,{ 0 }, 0},
		{ dwCountImages + STD_FILEOPEN, ID_FILE_OPEN,		TBSTATE_ENABLED, buttonStyles,{ 0 }, 0},
		{ dwCountImages + STD_FILESAVE, ID_FILE_SAVE,		TBSTATE_ENABLED, buttonStyles,{ 0 }, 0},
		{ 0,							0,					TBSTATE_ENABLED, TBSTYLE_SEP, {0}, 0},
		{ 0,							ID_DRAW_LINE,		TBSTATE_ENABLED, buttonStyles,{ 0 }, 0},
		{ 1,							ID_DRAW_RECTANGLE,	TBSTATE_ENABLED, buttonStyles,{ 0 }, 0 },
		{ 2,							ID_DRAW_ELLIPSE,	TBSTATE_ENABLED, buttonStyles,{ 0 }, 0 },
		{ 3,							ID_DRAW_TEXT,		TBSTATE_ENABLED, buttonStyles,{ 0 }, 0 }
	};

	// structure contains the bitmap of user defined buttons. It contains 2 icons


	// Add buttons.
	SendMessage(hWndToolbar, TB_BUTTONSTRUCTSIZE, (WPARAM)sizeof(TBBUTTON), 0);
	SendMessage(hWndToolbar, TB_ADDBUTTONS, (WPARAM)numButtons, (LPARAM)&tbButtons);

	// Resize the toolbar, and then show it.
	SendMessage(hWndToolbar, TB_AUTOSIZE, 0, 0);
	ShowWindow(hWndToolbar, TRUE);

	return hWndToolbar;
}

